package org.binar.challenge5.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.binar.challenge5.model.Users;
import org.binar.challenge5.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Tag(name = "Users", description = "API for processing with Users entity")
@RestController
@RequestMapping("/users")
@AllArgsConstructor
public class UsersController {

    @Autowired
    private UsersService usersService;

    @Operation(summary = "add user to the Users table in Bioskop Database")
    @PostMapping("/add-user")
    public Users addUser (@RequestBody Users user){
        return usersService.saveUser(user);
    }

    @Operation(summary = "delete user by user name from the Users table in Bioskop Database")
    @DeleteMapping("/{userName}")
    public String removeByID (@PathVariable("userName") String userName){
        usersService.removeUserByUserName(userName);
        return "Hapus User berhasil";
    }

    @Operation(summary = "get all users from the Users table in Bioskop Database")
    @GetMapping
    public Iterable<Users> findAll(){
        return usersService.findAllUser();
    }

    @Operation(summary = "get user by user name from the Users table in Bioskop Database")
    @GetMapping("/{userName}")
    public Users findUserByUserName (@PathVariable("userName") String userName){
        return usersService.findUserByUserName(userName);
    }

    @GetMapping("/tiket")
    public void getTiket(HttpServletResponse response,
                             @RequestParam("userName") String userName,
                             @RequestHeader("Authorization") String auth) throws IOException, JRException {
        System.out.println(auth);
        JasperReport sourceFileName = JasperCompileManager
                .compileReport(ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX
                        + "tiket.jrxml").getAbsolutePath());

        // creating our list of beans
        List<Users> dataList = new ArrayList<>();
//        List<Map<String, String>> dataList = new ArrayList<>();

        Map<String, String> data = new HashMap<>();
        Users user = usersService.findUserByUserName(userName);
//        data.put("username", us.getUsername());
//        data.put("email", us.getEmail());
//        data.put("typeName", us.getType_id().getTypeName());
//        data.put("cobaField", "Ini hanya cobaan");
//        dataList.add(data);

        dataList.add(user);

        // creating datasource from bean list
        JRBeanCollectionDataSource beanColDataSource = new JRBeanCollectionDataSource(dataList);
        Map<String, Object> parameters = new HashMap();
        parameters.put("createdBy", "Binar");
        JasperPrint jasperPrint = JasperFillManager.fillReport(sourceFileName, parameters, beanColDataSource);

        response.setContentType("application/pdf");
        response.addHeader("Content-Disposition", "inline; filename=tiket.pdf;");

        JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());
    }

}
