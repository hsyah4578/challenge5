package org.binar.challenge5.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.binar.challenge5.model.Seats;
import org.binar.challenge5.service.SeatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "Seats", description = "API for processing with Seats entity")
@RestController
@RequestMapping("/seats")
@AllArgsConstructor
public class SeatsController {

    @Autowired
    private SeatsService seatsService;

    @Operation(summary = "add seat in to the Seats table in Bioskop Database")
    @PostMapping("/add-seat")
    public Seats saveSeat (Seats seat){
        return seatsService.saveSeat(seat);
    }

    @Operation(summary = "show all seats from the Seats table in Bioskop Database")
    @GetMapping
    public Iterable <Seats> getAllSeats(){
        return seatsService.getAllSeats();
    }

}
