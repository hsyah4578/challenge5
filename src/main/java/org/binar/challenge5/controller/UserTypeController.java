package org.binar.challenge5.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.binar.challenge5.model.UserType;
import org.binar.challenge5.service.UserTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Tag(name = "User Type", description = "API for processing with User Type entity")
@RestController
@RequestMapping("/user-type")
@AllArgsConstructor
public class UserTypeController {

    @Autowired
    private UserTypeService userTypeService;

    @Operation(summary = "add user type into the UserType table in Bioskop Database")
    @PostMapping("/add-usertype")
    public UserType addUserType(@RequestBody UserType userType){
        return userTypeService.saveUserType(userType);
    }

    @Operation(summary = "delete user type from the UserType table in Bioskop Database")
    @DeleteMapping("/{typeId}")
    public String removeByID (@PathVariable("typeId") Integer typeId){
        userTypeService.removeById(typeId);
        return "Hapus User Type berhasil";
    }

    @Operation(summary = "get all user types from the UserType table in Bioskop Database")
    @GetMapping
    public Iterable<UserType> findAll(){
        return userTypeService.findAllUserType();
    }

    @Operation(summary = "get user type by typeId from the UserType table in Bioskop Database")
    @GetMapping("/{typeId}")
    public UserType findByTypeId (@PathVariable("typeId") Integer typeId){
        return userTypeService.findByTypeId(typeId);
    }
}