package org.binar.challenge5.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.binar.challenge5.model.Schedules;
import org.binar.challenge5.repository.SchedulesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/schedule")
@AllArgsConstructor
@Tag(name = "Schedule", description = "API for processing with Schedule entity")
public class SchedulesController {

    @Autowired
    private SchedulesRepository schedulesRepository;

    @Operation(summary = "add schedule to the Schedules table in Bioskop Database")
    @PostMapping("/add-schedule")
    public Schedules saveSchedule (@RequestBody Schedules schedule){
        return schedulesRepository.save(schedule);
    }

    @Operation(summary = "get all schedules from the Schedules table in Bioskop Database")
    @GetMapping
    public Iterable <Schedules> findAll(){
        return schedulesRepository.findAll();
    }

    @Operation(summary = "update schedule from the Schedules table in Bioskop Database")
    @PutMapping("/update")
    public Schedules updateScheduleByScheduleId (@RequestBody Schedules schedule){
        return schedulesRepository.save(schedule);
    }

    @Operation(summary = "get schedules by tanggal tayang from the Schedules table in Bioskop Database")
    @GetMapping("/tanggal-tayang/{tanggalTayang}")
    public List<Schedules> findScheduleByTanggalTayang ( @PathVariable("tanggalTayang") String tanggalTayang){
        return schedulesRepository.findScheduleByTanggalTayang(tanggalTayang);
    }

    @Operation(summary = "get schedules by judul film from the Schedules table in Bioskop Database")
    @GetMapping("/judul-film/{judulFilm}")
    public List<Schedules> findScheduleByJudulFilm (@PathVariable("judulFilm") String judulFilm){
        return schedulesRepository.findScheduleByJudulFilm(judulFilm);
    }

}
