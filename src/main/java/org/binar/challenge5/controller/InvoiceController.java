package org.binar.challenge5.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.binar.challenge5.service.InvoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@Tag(name = "Invoice and Tickets", description = "API for processing Invoice and ticket print")
@RestController
@RequestMapping("/invoice")
public class InvoiceController {

    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private HttpServletResponse response;

    @Operation(summary = "get list all invoice from Invoice table in the Bioskop Database")
    @GetMapping
    public void getTicketReport() throws Exception {
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; fileName=\"list_invoice.pdf\"");
        JasperPrint jasperPrint = invoiceService.allTicketInvoice();
        JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());
    }

    @Operation(summary = "get list invoice by user name froms Invoice table in Bioskop Database")
    @GetMapping("/{user_name}")
    public void getTicketReportWithParam(@PathVariable("user_name") String userName) throws Exception {
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; fileName=\"invoice.pdf\"");
        JasperPrint jasperPrint = invoiceService.invoiceByUserName(userName);
        JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());
    }

    @GetMapping("/print/{user_name}")
    public void getUserTickets(@PathVariable("user_name") String userName) throws Exception {
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; fileName=\"user_tickets.pdf\"");
        JasperPrint jasperPrint = invoiceService.ticketByUserName(userName);
        JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());
    }

    @Operation(summary = "Print ticket by nomor tiket")
    @GetMapping("/print/ticket/{nomor_tiket}")
    public void printTicket (@PathVariable("nomor_tiket") Integer nomorTiket) throws Exception {
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; fileName=\"ticket.pdf\"");
        JasperPrint jasperPrint = invoiceService.printTicket(nomorTiket);
        JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());
    }
}
