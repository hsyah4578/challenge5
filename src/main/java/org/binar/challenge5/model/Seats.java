package org.binar.challenge5.model;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@IdClass(StudioSeat.class)
public class Seats {

    @Id
    private String nomorKursi;
    @Id
    private String studio;

}
