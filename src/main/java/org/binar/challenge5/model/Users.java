package org.binar.challenge5.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity
public class Users implements Serializable {

    @Id
    private String userName;

    private String fullName;

    @Column(unique = true)
    private Long phone;

    @Column(unique = true)
    private String email;

    private String password;

    @ManyToOne
    @JoinColumn(name = "typeId")
    private UserType typeId;

}
