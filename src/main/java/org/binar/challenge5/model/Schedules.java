package org.binar.challenge5.model;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
public class Schedules implements Serializable {
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer scheduleId;

    @ManyToOne
    @JoinColumn(name = "kodeFilm")
    private Films kodeFilm;

    private String tanggalTayang;

    private String jamMulai;

    private String jamSelesai;

    private Float hargaTiket;
}
