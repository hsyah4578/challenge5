package org.binar.challenge5.model;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@Entity
public class TicketBooking implements Serializable {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Integer nomorTiket;

    @ManyToOne
    @JoinColumn(name = "userName")
    private Users userName;

    @ManyToOne
    @JoinColumn(name = "scheduleId")
    private Schedules scheduleId;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "nomorKursi", referencedColumnName = "nomorKursi"),
            @JoinColumn(name = "studio", referencedColumnName = "studio")
    })
    private Seats pesan;
}
