package org.binar.challenge5.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity
public class UserType implements Serializable {

    @Id
    private Integer typeId;

    @Column(unique = true)
    private String typeName;

}
