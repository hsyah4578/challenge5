package org.binar.challenge5.service;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@Service
public class InvoiceService {

    @Autowired
    private DataSource dataSource;

    private Connection getConnection(){
        try{
            return dataSource.getConnection();
        }catch (SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    public JasperPrint allTicketInvoice() throws Exception {
        InputStream fileReport = new ClassPathResource("reports/ListInvoice.jasper").getInputStream();
        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(fileReport);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, null, getConnection());
        return jasperPrint;
    }

    public JasperPrint invoiceByUserName(String paramName) throws Exception {
        InputStream fileReport = new ClassPathResource("reports/ListTicket.jasper").getInputStream();
        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(fileReport);
        Map<String, Object> parameter = new HashMap<String, Object>();
        parameter.put("userName", "%"+paramName+"%");
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameter, getConnection());
        return jasperPrint;
    }

    public JasperPrint ticketByUserName(String username) throws Exception {
        InputStream fileReport = new ClassPathResource("reports/PrintUserTickets.jasper").getInputStream();
        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(fileReport);
        Map<String, Object> parameter = new HashMap<String, Object>();
        parameter.put("userName", "%"+username+"%");
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameter, getConnection());
        return jasperPrint;
    }

    public JasperPrint printTicket(Integer paramTicket) throws Exception {
        InputStream fileReport = new ClassPathResource("reports/PrintTicket.jasper").getInputStream();
        JasperReport jasperReport = (JasperReport) JRLoader.loadObject(fileReport);
        Map<String, Object> parameter = new HashMap<String, Object>();
        parameter.put(String.valueOf("nomorTiket"), paramTicket);
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameter, getConnection());
        return jasperPrint;
    }
}
