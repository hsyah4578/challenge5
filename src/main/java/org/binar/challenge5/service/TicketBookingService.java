package org.binar.challenge5.service;

import org.binar.challenge5.model.TicketBooking;

public interface TicketBookingService {

    TicketBooking saveTicket(TicketBooking ticketBooking);

    Iterable<TicketBooking> findAllTickets();

}
