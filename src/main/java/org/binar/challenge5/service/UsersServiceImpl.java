package org.binar.challenge5.service;

import org.binar.challenge5.model.Users;
import org.binar.challenge5.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsersServiceImpl implements UsersService{

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private UserTypeService userTypeService;

    @Override
    public Users saveUser(Users user) {
        return usersRepository.save(user);
    }

    @Override
    public void removeUserByUserName(String userName) {
        usersRepository.deleteById(userName);
    }

    @Override
    public Iterable<Users> findAllUser() {
        return usersRepository.findAll();
    }

    @Override
    public Users findUserByUserName(String userName) {
        return usersRepository.findUserByUserName(userName);
    }

    @Override
    public Users findUserByEmail(String email) {
        return usersRepository.findUserByEmail(email);
    }


}
