package org.binar.challenge5.service;

import org.binar.challenge5.model.Films;
import org.binar.challenge5.repository.FilmsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FilmsServiceImpl implements FilmsService{

    @Autowired
    private FilmsRepository filmsRepository;

    @Override
    public Films saveFilm(Films film) {
        return filmsRepository.save(film);
    }

    @Override
    public void removeFilmByKodeFilm(String kodeFilm) {
        filmsRepository.deleteById(kodeFilm);
    }

    @Override
    public Iterable<Films> findAllFilms() {
        return filmsRepository.findAll();
    }

    @Override
    public Films findFilmByKodeFilm(String kodeFilm) {
        return filmsRepository.findFilmByKodeFilm(kodeFilm);
    }

    @Override
    public Films findFilmByJudulFilm(String judulFilm) {
        return filmsRepository.findFilmByKodeFilm(judulFilm);
    }

    @Override
    public List<Films> findFilmBySedangTayang() {
        return filmsRepository.findFilmBySedangTayang();
    }
}
