package org.binar.challenge5.service;

import org.binar.challenge5.model.Films;

import java.util.List;

public interface FilmsService {

    Films saveFilm (Films film);

    void removeFilmByKodeFilm (String kodeFilm);

    Iterable <Films> findAllFilms();

    Films findFilmByKodeFilm(String kodeFilm);

    Films findFilmByJudulFilm(String judulFilm);

    List<Films> findFilmBySedangTayang();

}
