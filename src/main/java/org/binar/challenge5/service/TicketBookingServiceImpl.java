package org.binar.challenge5.service;

import org.binar.challenge5.model.TicketBooking;
import org.binar.challenge5.repository.InvoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TicketBookingServiceImpl implements TicketBookingService {

    @Autowired
    private InvoiceRepository invoiceRepository;


    @Override
    public TicketBooking saveTicket(TicketBooking ticketBooking) {
        return invoiceRepository.save(ticketBooking);
    }

    @Override
    public Iterable<TicketBooking> findAllTickets() {
        return invoiceRepository.findAll();
    }
}
