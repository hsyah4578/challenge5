package org.binar.challenge5.service;

import org.binar.challenge5.model.Users;

public interface UsersService {

        Users saveUser (Users user);

        void removeUserByUserName (String userName);

        Iterable <Users> findAllUser ();

        Users findUserByUserName (String userName);

        Users findUserByEmail (String email);

}
