package org.binar.challenge5.service;

import org.binar.challenge5.model.Seats;

public interface SeatsService {

    Seats saveSeat (Seats seats);

    Iterable<Seats> getAllSeats();

}
