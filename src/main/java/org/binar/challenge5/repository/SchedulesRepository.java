package org.binar.challenge5.repository;

import org.binar.challenge5.model.Schedules;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SchedulesRepository extends JpaRepository <Schedules, Integer> {

    @Query("select s from Schedules s where s.tanggalTayang = :tanggalTayang")
    List<Schedules> findScheduleByTanggalTayang(@Param("tanggalTayang") String tanggalTayang);

    @Query("select s from Schedules s " +
            "join Films f on f.kodeFilm = s.kodeFilm " +
            "where f.judulFilm like %:judulFilm%")
    List<Schedules> findScheduleByJudulFilm(@Param("judulFilm") String judulFilm);

}
