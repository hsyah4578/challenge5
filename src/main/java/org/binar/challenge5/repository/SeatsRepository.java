package org.binar.challenge5.repository;

import org.binar.challenge5.model.StudioSeat;
import org.binar.challenge5.model.Seats;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SeatsRepository extends JpaRepository<Seats, StudioSeat> {

}
