package org.binar.challenge5.repository;

import org.binar.challenge5.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends JpaRepository <Users, String>{

    @Query("select u from Users u where u.userName = :userName")
    Users findUserByUserName (@Param("userName") String userName);

    @Query("select u from Users u where u.email = :email")
    Users findUserByEmail (@Param("email") String email);

}
