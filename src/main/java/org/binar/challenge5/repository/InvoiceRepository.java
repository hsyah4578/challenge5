package org.binar.challenge5.repository;

import org.binar.challenge5.model.TicketBooking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface InvoiceRepository extends JpaRepository<TicketBooking, Integer> {

    @Query(nativeQuery = true, value = "select * from invoice i where i.user_name = :userName")
    TicketBooking findTiketByUserName (@Param("userName") String userName);

    @Query(value = "select i.nomor_tiket, i.nomor_kursi, i.studio, i.user_name, s.tanggal_tayang, " +
            "s.jam_mulai, s.harga_tiket, f.judul_film, i.schedule_id " +
            "from invoice i join schedules s on i.schedule_id = s.schedule_id join films f on s.kode_film = f.kode_film " +
            "where i.nomor_tiket = :nomorTiket", nativeQuery = true)
    TicketBooking findTiketByNomorTiket (@Param("nomorTiket") Integer nomotTiket);


}
