package org.binar.challenge5.repository;

import org.binar.challenge5.model.UserType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserTypeRepository extends JpaRepository <UserType, Integer> {

    @Query("select ut from UserType ut where ut.typeId = :typeId")
    UserType findByTypeId(@Param("typeId") Integer typeId);

    @Query("select ut from UserType ut where ut.typeName = :typeName")
    UserType findByTypeName(@Param("typeName") String typeName);

}
